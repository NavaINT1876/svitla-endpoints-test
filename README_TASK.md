# Coding challenge for Backend Developer (PHP)

Hello dear candidate,

we are pleased you are reading this, which means your profile attracted us 
and we are keen to see your skills in practice.

For this we kindly ask you to demonstrate your solution for the following situation.

- You need to develop an application (RESTful API) written mainly with PHP.
- The API must have two endpoints. First endpoint is for creation (registration) of a new user 
  and should consume the following data: email, first name, last name, password.   
  Second endpoint is for authorization against the API, using email/password combination.

Treat this task as you would like to introduce Auth service for your company or your own project.
Think about which best practices you would like to apply.
Feel free to use (or not to use) any frameworks, libraries, technologies.

**We understand that architecting even a small service will normally take more time than a typical code task should.
  Therefore, please only provide as much as possible within reasonable time (3-5 hours). 
  In this case more important would be to share what would you improve and develop further, 
  if you would have unlimited time. Please write a few notes about it.**
  
When you are done, please submit a pull request.
