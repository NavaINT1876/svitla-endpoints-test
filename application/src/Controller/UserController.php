<?php

namespace App\Controller;

use App\Exception\InvalidAuthorizationParametersException;
use App\Exception\InvalidUserParametersException;
use App\Service\AuthorizationService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * Class UserController
 *
 * @Route("/api/v1/user")
 *
 * @package App\Controller
 */
class UserController extends AbstractController
{
    /**
     * @Route("", name="user_create", methods={"POST"})
     * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     description="Raw json body with user email, first name, last name and password.",
     *     required=true,
     *     type="string",
     *     @SWG\Schema(
     *         type="string",
     *         example="{
    ""email"": ""kutscherenko.igor@gmail.com"",
    ""firstName"": ""Ihor"",
    ""lastName"": ""Kucherenko"",
    ""password"": ""i3948jf98jsS$Akd""
}"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Created."
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Failed."
     * )
     * @param UserService $userService
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(UserService $userService, Request $request)
    {
        try {

            $data = json_decode($request->getContent(), true);
            $userService->create($data);

            return $this->json(['message' => 'Created.']);
        } catch (InvalidUserParametersException $exception) {
            return $this->json(['message' => 'Failed.' . $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return $this->json(['message' => 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/authorize", name="user_authorize", methods={"POST"})
     * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     description="Raw json body with email and password.",
     *     required=true,
     *     type="string",
     *     @SWG\Schema(
     *         type="string",
     *         example="{
    ""email"": ""kutscherenko.igor@gmail.com"",
    ""password"": ""i3948jf98jsS$Akd""
}"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Authorized."
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Failed."
     * )
     * @param AuthorizationService $authService
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function authorize(AuthorizationService $authService, Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);

            $token = $authService->getToken($data);

            return $this->json(['message' => 'Authorized.', 'token' => $token]);
        } catch (InvalidAuthorizationParametersException $exception) {
            return $this->json(['message' => 'Failed. ' . $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return $this->json(['message' => 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
