<?php

namespace App\Service;

use App\Entity\User;
use DateTimeImmutable;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class TokenGenerator
 * @package App\Service
 */
class TokenGenerator
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function generate(User $user): string
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), new Key($this->params->get('jwt_key')));

        $now = new DateTimeImmutable();
        $token = $config->createBuilder()
            ->issuedBy('http://svitla.test.task.com')
            ->permittedFor('http://svitla.test.task.com')
            ->identifiedBy(random_int(1, 100000))
            ->issuedAt($now)
            ->expiresAt($now->modify('+1 hour'))
            ->withClaim('uid', $user->getId())
            ->withClaim('first_name', $user->getFirstName())
            ->withClaim('last_name', $user->getLastName())
            ->withClaim('email', $user->getEmail())
            ->getToken($config->getSigner(), $config->getSigningKey());

        return (string)$token;
    }
}
