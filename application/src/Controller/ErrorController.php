<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ErrorController
 *
 * @package App\Controller
 */
class ErrorController extends AbstractController
{
    public function show(\Throwable $exception)
    {
        return $this->json([
            'code' => $exception->getCode(),
            'message' => $exception->getMessage()
        ]);
    }
}
