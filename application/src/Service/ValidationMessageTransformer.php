<?php

namespace App\Service;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Class ValidationMessageTransformer
 * @package App\Service
 */
class ValidationMessageTransformer
{
    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     * @return string
     */
    public function transform(ConstraintViolationListInterface $constraintViolationList): string
    {
        $message = '';
        foreach ($constraintViolationList as $violation) {
            $message .= ' ' . $violation->getMessage();
        }

        return $message;
    }
}

