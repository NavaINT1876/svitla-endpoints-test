<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity
 * @UniqueEntity(fields={"email"}, message="This email has already been taken.")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Assert\NotBlank(message="Email should not be blank.")
     * @Assert\Email(message="Email has invalid format.")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="First Name should not be blank.")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "First Name must be at least {{ limit }} characters long.",
     *      maxMessage = "First Name cannot be longer than {{ limit }} characters.",
     *      allowEmptyString = false
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Last Name should not be blank.")
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "Last Name must be at least {{ limit }} characters long.",
     *      maxMessage = "Last Name cannot be longer than {{ limit }} characters.",
     *      allowEmptyString = false
     * )
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Password should not be blank.")
     * @Assert\Length(
     *      min = 6,
     *      max = 255,
     *      minMessage = "Password must be at least {{ limit }} characters long.",
     *      maxMessage = "Password cannot be longer than {{ limit }} characters.",
     *      allowEmptyString = false
     * )
     * @Assert\Regex(
     *     "/^(?=.*[!@#$%^&*-])(?=.*[0-9])(?=.*[A-Z]).{6,255}$/",
     *     message="Password should match the following requirements: a minimum of 6 characters, at least one uppercase letter, at least one number (digit), at least one of the following special characters !@#$%^&*-."
     * )
     */
    private $password;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getRoles()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return '';
    }

    /**
     * @return string|null
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * @return void
     */
    public function eraseCredentials()
    {

    }
}
