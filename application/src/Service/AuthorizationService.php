<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\InvalidAuthorizationParametersException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AuthorizationService
 * @package App\Service
 */
class AuthorizationService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /** @var TokenGenerator */
    private $tokenGenerator;

    /**
     * AuthorizationService constructor.
     *
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $userPasswordEncoder,
        TokenGenerator $tokenGenerator
    )
    {
        $this->em = $em;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param array $data
     * @return string
     * @throws InvalidAuthorizationParametersException
     */
    public function getToken(array $data): string
    {
        if (empty($data['email']) || empty($data['password'])) {
            throw new InvalidAuthorizationParametersException('Both fields "email" and "password" should be provided.');
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
        if (!$user) {
            throw new InvalidAuthorizationParametersException(sprintf('User with email %s was not found.', $data['email']));
        }

        if (!$this->userPasswordEncoder->isPasswordValid($user, $data['password'])) {
            throw new InvalidAuthorizationParametersException('Wrong password.');
        }

        return $this->tokenGenerator->generate($user);
    }
}
