# Install Project

Execute command in the root folder in order to build project infrastructure:

```docker-compose up -d --build```

Then run command below in order to install composer dependencies:

```docker exec svitla-ihor-web composer install --prefer-dist --no-suggest```

Last thing to do is to run command in order to apply migration and create user table:

```docker exec svitla-ihor-web php bin/console doctrine:migrations:migrate -n```

After that you will be able to visit url web interface http://localhost:9153/api/doc, see documentation and test endpoints manually.

Also you will be able to visit page http://localhost:9155 and see database management console and DB "svitla" with table "user" and see created records.
