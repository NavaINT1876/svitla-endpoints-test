<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\InvalidUserParametersException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UserService
 * @package App\Repository
 */
class UserService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ValidatorInterface */
    private $validator;

    /** @var ValidationMessageTransformer */
    private $validationMessageTransformer;

    /** @var UserPasswordEncoderInterface */
    private $userPasswordEncoder;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     * @param ValidationMessageTransformer $validationMessageTransformer
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator,
        ValidationMessageTransformer $validationMessageTransformer,
        UserPasswordEncoderInterface $userPasswordEncoder
    )
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->validationMessageTransformer = $validationMessageTransformer;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param array|null $data
     * @throws InvalidUserParametersException
     */
    public function create(?array $data): void
    {
        $user = (new User())
            ->setEmail($data['email'] ?? '')
            ->setFirstName($data['firstName'] ?? '')
            ->setLastName($data['lastName'] ?? '')
            ->setPassword($data['password'] ?? '');

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            $errorMessage = $this->validationMessageTransformer->transform($errors);

            throw new InvalidUserParametersException($errorMessage);
        }

        $user->setPassword($this->userPasswordEncoder->encodePassword($user, $data['password']));

        $this->em->persist($user);
        $this->em->flush();
    }
}
