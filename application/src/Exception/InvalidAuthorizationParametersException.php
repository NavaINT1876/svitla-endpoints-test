<?php

namespace App\Exception;

/**
 * Class InvalidAuthorizationParametersException
 * @package App\Exception
 */
class InvalidAuthorizationParametersException extends \Exception
{

}
